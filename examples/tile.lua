-- SCALEINATOR EXAMPLE: Proper Tiles (with Love2D)
-- Copyright 2016 Dominik "Zatherz" Banaszak
-- Licensed under the MIT license: https://opensource.org/licenses/MIT

scaleinator = require("scaleinator")
scale = scaleinator.create()

local tilesize = 16 -- Width and height of a "tile" is 16
local gridsize = 16 -- The grid consists of 16x16 tiles

function love.load()
	love.window.setMode(640, 480, {resizable = true})
	love.window.setTitle("Scaleinator + Love2D Example with Tiles")
	scale:newmode("default", gridsize * tilesize, gridsize * tilesize)
	scale:update(640, 480)
end

function love.resize(w, h)
	scale:update(w, h)
end

function love.draw()
	local tx, ty = scale:gettranslation()
	local bw, bh = scale:getbox()
	love.graphics.translate(tx, ty)
	love.graphics.rectangle("fill", 0, 0, bw, bh)
	love.graphics.setColor(123, 123, 123)
	love.graphics.rectangle("fill", 0, 0, tilesize * scale:getfactor(), tilesize * scale:getfactor()) -- Draw a single gray tile in the top left corner of our "game area"
	love.graphics.setColor(255, 255, 255)
end
