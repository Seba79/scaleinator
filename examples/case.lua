-- SCALEINATOR EXAMPLE: Adapting to your case style (based on box.lua)
-- Copyright 2016 Dominik "Zatherz" Banaszak
-- Licensed under the MIT license: https://opensource.org/licenses/MIT

-- Scaleinator's functions are case insensitive meaning that you can adapt it to your code by using whichever case style you prefer.
-- Scaleinator's handling of case insensitivity even extends as far as completely ignoring underscores, making it so that you can use snake case (something_like_this).

scaleinator = require "scaleinator"
screen = scaleinator.create()
screen:newMode("blah", 1, 1) -- camelCase
screen:SetMode("blah") -- PascalCase
screen:updateresolution(640, 480) -- all lower case
print("Mode: blah (1:1), screen size: 640x480")
screen:process()
print("Box W:", screen:get_box_w(), "Box H:", screen:get_boxh()) -- different forms of snake case
