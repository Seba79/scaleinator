-- SCALEINATOR EXAMPLE: Box
-- Copyright 2016 Dominik "Zatherz" Banaszak
-- Licensed under the MIT license: https://opensource.org/licenses/MIT

scaleinator = require "scaleinator"
screen = scaleinator.create()
screen:newmode("blah", 1, 1)
screen:setmode("blah")
screen:updateresolution(640, 480)
print("Mode: blah (1:1), screen size: 640x480")
screen:process()
print("Box W:", screen:getboxw(), "Box H:", screen:getboxh())
