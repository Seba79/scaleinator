-- SCALEINATOR EXAMPLE: Advanced Box
-- Copyright 2016 Dominik "Zatherz" Banaszak
-- Licensed under the MIT license: https://opensource.org/licenses/MIT

scaleinator = require "scaleinator"
screen = scaleinator.create()
screen:newmode("16:9", 16, 9)
screen:setmode("16:9")
screen:updateresolution(32, 20)
screen:process()
print("Mode: 16:9, screen size: 32x20")
print("SW:", 32, "SH:", 20, "X:", screen:gettranslationx(), "Y:", screen:gettranslationy(), "W:", screen:getboxw(), "H:", screen:getboxh())
screen:setyhandling("-")
screen:process()
print("SW:", 32, "SH:", 20, "X:", screen:gettranslationx(), "Y:", screen:gettranslationy(), "W:", screen:getboxw(), "H:", screen:getboxh())
