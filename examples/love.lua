-- SCALEINATOR EXAMPLE: Integrating with Love2D
-- Copyright 2016 Dominik "Zatherz" Banaszak
-- Licensed under the MIT license: https://opensource.org/licenses/MIT

scaleinator = require("scaleinator")
scale = scaleinator.create()

function love.load()
	love.window.setMode(640, 480, {resizable = true})
	love.window.setTitle("Scaleinator + Love2D Example")
	scale:newmode("16:9", 16, 9) -- Create a mode with 16:9 aspect ratio. The first created mode is automatically set.
	scale:update(love.graphics.getWidth(), love.graphics.getHeight()) -- Update the scale object, providing the width and height of the screen.
        -- The first time a call to update() is done, the width and height are saved as the "original" size.
        -- These values are later used for functions getresizefactor and getoriginalfactor.
end

function love.resize(w, h)
	scale:update(w, h) -- On resize, update the scale object's inner resolution. This resolution will never be set as the original resolution, because the update() in love.load gets run first.
end

function love.draw()
        love.graphics.setColor(255, 255, 255)
	local tx, ty = scale:gettranslation() -- Get the translation of the 16:9 box area.
	local bw, bh = scale:getbox() -- Get the width and height of the 16:9 box area.
        love.graphics.translate(tx, ty) -- Translate the coordinate system by the 16:9 box values.
	love.graphics.rectangle("fill", 0, 0, bw, bh) -- Create a rectangle from 0,0 to the width and height of the box area. The coordinate system is translated, so 0,0 is actually tx,ty.
        love.graphics.setColor(123, 123, 123)
        local bsw = scale:getresizefactor() * 100 -- Get the scaled output width of width 100
        local bsh = scale:getresizefactor() * 100 -- Get the scaled output height of height 100
        love.graphics.rectangle("fill", bw/2-bsw/2, bh/2-bsh/2, bsw, bsh) -- Draw a dynamically scaled second rectangle in the center of the box area
end
